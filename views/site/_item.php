
<div class="row">
    <div class="blog-item col-md-8 col-md-offset-2">
        <b><?= $model->title ?></b>
        <div class="row">
            <div><img class="col-md-8 col-md-offset-2" src="https://www.webpagefx.com/blog/images/assets/cdn.sixrevisions.com/0224-20_beautiful_blog_design_ricebowls.jpg"></div>
        </div>
        <div><?= $model->text ?></div>
        <div class="text-right"><i>Created</i> <?= $model->created ?></div>
    </div>
</div>