<?php

use yii\widgets\ListView;
?>
<style>
    .blog-item{
        border-radius: 10px;
        margin-bottom: 10px;
    }
</style>
<div class="blog">
    <?=
    ListView::widget([
        'dataProvider' => $provider,
        'itemView' => '_item',
        'layout' => '<div class="items">{items}</div><div>{pager}</div>'
    ])
    ?>
</div>

