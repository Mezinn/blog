<?php

namespace app\controllers\api;

use Yii;
use yii\rest\ActiveController;

class BlogArticlesController extends ActiveController {

    public $modelClass = 'app\models\BlogArticles';

}
