<?php

use yii\db\Migration;
use app\models\BlogArticles;

/**
 * Class m180716_135039_blog
 */
class m180716_135039_blog extends Migration {

    public function up() {
        $this->createTable('blog_articles', [
            'uid' => $this->primaryKey(),
            'title' => $this->string(),
            'text' => $this->text(),
            'created' => $this->dateTime(),
            'updated' => $this->dateTime(),
        ]);

        foreach (range(0, 100) as $it) {
            $faker = Faker\Factory::create();
            $item = new BlogArticles(['title' => $faker->name, 'text' => $faker->text]);
            $item->save();
        }

        return true;
    }

    public function down() {
        return $this->dropTable('blog_articles');
    }

}
